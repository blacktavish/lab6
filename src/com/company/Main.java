package com.company;

public class Main {

    public static void main(String[] args) {

        Quiz quiz = new QuizImpl();
        int minRange = Quiz.MIN_VALUE;
        int maxRange = Quiz.MAX_VALUE;
        int digit = (minRange + maxRange) / 2;

        for(int counter = 1; ;counter++) {
            try {
                quiz.isCorrectValue(digit);
                System.out.println("Nice try!!! Searching value: "
                        + digit + "Number of attempts:  " + counter);
                break;
            } catch (Quiz.ParamTooLarge e) {
                System.out.println("Argument too large!!! " + digit);
                maxRange = digit;
                digit = (minRange + maxRange) / 2;

            } catch (Quiz.ParamTooSmall e) {
                System.out.println("Argument too small!!! " + digit);
                minRange = digit;
                digit = (minRange + maxRange) / 2;
            }
        }

    }
}
