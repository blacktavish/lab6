package com.company;

public class QuizImpl implements Quiz {

    private int digit;
    public QuizImpl(){
        this.digit = 150;
    }
    public void isCorrectValue(int value) throws ParamTooLarge, ParamTooSmall {
        if(value < this.digit){
            throw new ParamTooSmall();
        }
        else if(value > this.digit){
            throw new ParamTooLarge();
        }
    }
}
